import {Feature} from "ol";

export interface FlightPlan {
  markers: Feature[];
  lines: Feature[];
}
