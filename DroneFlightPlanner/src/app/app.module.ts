import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app-component/app.component';
import { MapComponent } from './map/map.component';
import { RightPanelComponent } from './right-panel/right-panel.component';
import { DroneFlightPlannerComponent } from './drone-flight-planner/drone-flight-planner.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    RightPanelComponent,
    DroneFlightPlannerComponent
  ],
    imports: [
        BrowserModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
