import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DroneFlightPlannerComponent } from './drone-flight-planner.component';

describe('DroneFlightPlannerComponent', () => {
  let component: DroneFlightPlannerComponent;
  let fixture: ComponentFixture<DroneFlightPlannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DroneFlightPlannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DroneFlightPlannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
