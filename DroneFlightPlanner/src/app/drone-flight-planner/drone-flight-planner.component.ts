import {Component, OnInit, ViewChild} from '@angular/core';
import {FlightPlan} from "../../model/flight-plan";
import {Feature} from "ol";
import {MapComponent} from "../map/map.component";

@Component({
  selector: 'app-drone-flight-planner',
  templateUrl: './drone-flight-planner.component.html',
  styleUrls: ['./drone-flight-planner.component.less']
})
export class DroneFlightPlannerComponent implements OnInit {
  @ViewChild(MapComponent) mapComponent!: MapComponent;

  allFlightPlans: FlightPlan[] = [];
  selectedFlightPlan: FlightPlan = {markers: [], lines: []};

  constructor() {
  }

  ngOnInit(): void {
  }

  startPlanning() {
    this.cleanSelectedFlightPlan();
    this.mapComponent.addMarkerPlacingFunctionality();
    this.mapComponent.addLineDrawingFunctionality();
  }

  finishPlanning() {
    if (!this.mapComponent) {
      return;
    }
    this.addNewFlightPlan(this.mapComponent.markerFeatures, this.mapComponent.lineFeatures);
    this.mapComponent.removeFunctionalities();
  }

  addNewFlightPlan(points: Feature[], lines: Feature[]) {
    this.allFlightPlans.push({markers: points, lines});
  }

  cleanSelectedFlightPlan() {
    this.selectedFlightPlan = {markers: [], lines: []};
  }

}
