import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {FlightPlan} from "../../model/flight-plan";

@Component({
  selector: 'app-right-panel',
  templateUrl: './right-panel.component.html',
  styleUrls: ['./right-panel.component.less']
})
export class RightPanelComponent implements OnInit {
  @Input() allFlightPlans: FlightPlan[] = []
  @Input() selectedFlightPlan: FlightPlan = {} as FlightPlan;

  @Output() selectedFlightPlanChange = new EventEmitter<FlightPlan>();
  @Output() planningStarted = new EventEmitter();
  @Output() planningFinished = new EventEmitter();

  isPlanning = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  startPlanning() {
    this.isPlanning = true;
    this.planningStarted.emit();
  }

  stopPlanning() {
    this.isPlanning = false;
    this.planningFinished.emit();
  }

  selectFlightPlan(flightPlan: FlightPlan) {
    if (this.isPlanning) {
      return;
    }
    this.selectedFlightPlan = flightPlan;
    this.selectedFlightPlanChange.emit(this.selectedFlightPlan);
  }
}
