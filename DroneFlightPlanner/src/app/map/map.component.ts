import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Feature, Map, MapBrowserEvent, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import {Icon, Style} from "ol/style";
import IconAnchorUnits from "ol/style/IconAnchorUnits";
import VectorLayer from "ol/layer/Vector";
import {Vector} from "ol/source";
import {Point} from "ol/geom";
import {toLonLat, transform} from "ol/proj";
import {Draw} from "ol/interaction";
import GeometryType from "ol/geom/GeometryType";
import VectorSource from "ol/source/Vector";
import {FlightPlan} from "../../model/flight-plan";
import {Coordinate} from "ol/coordinate";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.less']
})
export class MapComponent implements OnInit {

  @Output() addNewFlightPlan = new EventEmitter<FlightPlan>();

  markerSource = new Vector();
  markerLayer!: VectorLayer;

  lineSource = new VectorSource();
  lineLayer = new VectorLayer({source: this.lineSource});

  map!: Map;

  markerFeatures: Feature[] = [];
  lineFeatures: Feature[] = [];
  lineDraw!: Draw;

  addMarkerHandler = (evt: MapBrowserEvent) => {
    this.addMarker(toLonLat(evt.coordinate));
  };

  @Input()
  set mapFeatures(selectedFlightPlan: FlightPlan) {
    this.lineFeatures = selectedFlightPlan.lines;
    this.markerFeatures = selectedFlightPlan.markers;

    this.markerSource.clear();
    this.markerSource.addFeatures(this.markerFeatures);

    this.lineSource.clear();
    this.lineSource.addFeatures(this.lineFeatures);
  }

  constructor() {
  }

  ngOnInit(): void {
    this.initializeMap();
  }

  initializeMap() {
    const markerStyle = new Style({
      image: new Icon(({
        anchor: [0.5, 46],
        anchorXUnits: IconAnchorUnits.FRACTION,
        anchorYUnits: IconAnchorUnits.PIXELS,
        src: 'https://openlayers.org/en/v6.3.1/examples/data/icon.png'
      }))
    });
    this.markerLayer = new VectorLayer({
      source: this.markerSource,
      style: markerStyle
    });

    this.map = new Map({
      layers: [new TileLayer({source: new OSM()}),
        this.markerLayer,
        this.lineLayer
      ],
      view: new View({center: [0, 0], zoom: 0}),
      target: 'map'
    });

    this.lineDraw = new Draw({
      source: this.lineSource,
      type: GeometryType.LINE_STRING
    });
  }

  addMarker(lonLat: Coordinate): void {
    const [lon, lat] = lonLat
    const iconFeature = new Feature({
      geometry: new Point(transform(
        [lon, lat],
        'EPSG:4326',
        'EPSG:3857')),
    });
    this.markerFeatures.push(iconFeature);
    this.markerSource.addFeature(iconFeature);
  }

  removeFunctionalities(): void {
    this.lineDraw.finishDrawing();
    this.map.un('singleclick', this.addMarkerHandler);
    this.map.removeInteraction(this.map.getInteractions().pop());
    this.lineSource.getFeatures().forEach(lineFeature => this.lineFeatures.push(lineFeature));
  }

  addMarkerPlacingFunctionality(): void {
    this.map.on('singleclick', this.addMarkerHandler);
  }

  addLineDrawingFunctionality(): void {
    this.map.addInteraction(this.lineDraw);
  }

}
