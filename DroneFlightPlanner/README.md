# Drone Flight Planner

Web application to allow their customers to create and
store flight plans for their drones. When the application is loaded, it shows a map
and in one side of the page, the list of the flight plans already stored. The user will
be able to create a flight plan by clicking on the image/map and inserting points.
Each point will be joint to the next one inserted with a straight line. Once the user
has inserted all the points, he will be able to save the flight plan and load it at any
time over the image/map.

Written in Angular 10 using OpenLayers

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
